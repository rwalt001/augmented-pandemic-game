# README #

This is a game that combines augmented reality and the idea of escaping from a pandemic.

https://augmented-pandemic.herokuapp.com/game/

### What is this repository for? ###

This is multiplayer game that relies on the user wits and endurance to win

Game Overview

Your character has been given a virus by a mad scientist. As time progresses, it becomes more difficult for you to function the longer you live without treatment. Your mission is to build a cure by finding ingredients at different locations on the map. Time is against you. If it takes you too much time to reach your destination, your health will decrease and this will make it harder for you to get to your next destination. The virus can spontaneuously mutate and will hinder your progression. It is imperative that you make it to your destination checkpoints in a timely manner to build the cure for your virus.



* Product Owner: Ryan Walters
* SCRUM: Sharon Huang
* Developer: Ted Shin
